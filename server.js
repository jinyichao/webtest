var express = require('express');
var app = express();
app.use(express.static(__dirname + '/production/css'));
app.use('/vendors', express.static(__dirname + '/vendors'));


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/production/index.html');
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});